from argparse import ArgumentParser
from datetime import datetime
import json
import logging
from threading import Thread
import time

import requests
import requests.exceptions


LOGGING_FIELDS = 'time, method, url, code, elapsed, error'

TIME_UNITS_IN_SECS = {
    'min': 60,
    'sec': 1,
}

NUMBER_SUFFIX_TO_MULTIPLIER = {
    'k': 1e3,
    'm': 1e6,
}


logging.basicConfig(level=logging.INFO,
                    format='%(message)s')

logger = logging.getLogger(__file__)


def log(method, url, code, elapsed, exc=None,
        delim=' | ', log_keys=LOGGING_FIELDS):
    time = datetime.now().isoformat()
    elapsed = '%.2fs' % elapsed
    error = ''
    if exc:
        error = (f'{exc.__class__.__name__}: {exc}'
                if isinstance(exc, Exception)
                else f'({exc})')
    var = locals()
    args = filter(
        None,
        [var.get(s) for s in map(str.strip, log_keys.split(','))]
    )
    logger.info(delim.join(map(str, args)))


def req(method, url, data=None):
    start = time.time()
    error = None
    try:
        status = -1
        res = method(url, data=data, timeout=2)
        status = res.status_code

        if not res.ok:
            res.raise_for_status()

    except requests.exceptions.Timeout as exc:
        error = 'timed out'

    except BaseException as exc:
        error = exc

    finally:
        elapsed = time.time() - start
        method = method.__name__.upper()
        log(method, url, status, elapsed, error)


def parse_method(method, default=requests.get):
    return {
        'POST': requests.post,
        'GET': requests.get,
    }.get(method.upper(), default)


def parse_number(s):
    number = 0
    prefix, suffix = s[:-1], s[-1]
    if suffix.lower() in NUMBER_SUFFIX_TO_MULTIPLIER:
        number = float(prefix) * NUMBER_SUFFIX_TO_MULTIPLIER[suffix]
    else:
        number = float(s)
    return number


def parse_rate(rate='50/sec'):
    try:
        number, window = 50, 'sec'
        number, window = rate.split('/')

        window = window.lower().strip()
        if window not in TIME_UNITS_IN_SECS:
            raise ValueError('window must be "sec" or "min" e.g. 50/sec')
        
        number = parse_number(number)

        return int(number), window

    except ValueError as err:
        raise ValueError(f'bad argument for rate ({err})')
    

def run(method='GET',
        url='http://localhost',
        data=None,
        count=100,
        rate='50/sec',
        burst=False):
    # Calculate request emission rate
    freq, window = parse_rate(rate)
    window_in_secs = TIME_UNITS_IN_SECS[window]
    interval = window_in_secs / freq

    method = parse_method(method)
    done_counter = 0

    # Wrapper to count requests sent
    def counter_wrapper(*args):
        def wrapped():
            nonlocal args, done_counter
            func, *args = args
            res = func(*args)
            done_counter += 1
            return res
        return wrapped

    # Send requests
    threads = [
        Thread(target=counter_wrapper(req, method, url, data))
        for _ in range(count)
    ]
    try:
        start_time = time.time()
        for i, t in enumerate(threads):
            # Send request
            t.start()

            # No sleep if this is last request to send
            if i == len(threads):
                pass

            # Sleep for interval if smoothing (non-burst) is used
            if not burst:
                time.sleep(interval)
            
            # If burst is used, only sleep at the Nth request, where N is a
            # multiple of the frequency
            elif burst and (i + 1) % freq == 0:
                time.sleep(window_in_secs)

        # Wait for all threads to complete
        for t in threads:
            t.join()

    except KeyboardInterrupt:
        print('Interrupted, aborting.')

    finally:
        # Print some stats
        elapsed = time.time() - start_time
        time.sleep(0.5)
        
        actual_rate = done_counter / elapsed
        unit = 'sec'
        if actual_rate < 0.9:
            actual_rate *= 60
            unit = 'min'
        
        logger.info(f'made {done_counter} requests in {elapsed:.2f} sec '
                    f'(requested: {rate}, actual: {actual_rate:.2f}/{unit})')


if __name__ == '__main__':
    parser = ArgumentParser('loadtest')
    parser.add_argument('METHOD', type=str, default='GET',
                        help='HTTP method like GET, POST')
    parser.add_argument('URL', type=str, help='URL to call')
    parser.add_argument('-d', '--data', type=str, metavar='EXPR_JSON',
                        help='request body as eval-able Python expression, or '
                             'object encoded as JSON string')
    parser.add_argument('-n', '--number', default='100', metavar='INT',
                        help='number of requests')
    parser.add_argument('-r', '--rate', type=str, default='50/sec',
                        help='rate of requests, e.g. 50/sec, 2.5k/min')
    parser.add_argument('-b', '--burst', action='store_true',
                        help='dispatch all requests immediately at the start '
                             'of the given rate window instead of spreading '
                             'them out evenly over the second/minute')
    parser.add_argument('-l', '--log', type=str, metavar='FIELDS',
                        default=LOGGING_FIELDS,
                        help='comma-seperated list of fields to log')

    args = parser.parse_args()
    args.METHOD = args.METHOD.upper()
    args.number = int(parse_number(args.number))

    data = None
    if args.data:
        data = eval(args.data)
        if isinstance(data, str):
            try:
                data = json.loads(data)
            except json.JSONDecodeError:
                pass
    run(args.METHOD, args.URL, data,
        count=args.number, rate=args.rate, burst=args.burst)
