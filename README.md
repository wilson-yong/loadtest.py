# loadtest.py

A crude load-testing script in Python.

## Usage

```bash
pip install requests   # Install dependencies

python3 loadtest.py POST http://localhost:8080/submit \
    -d '{"foo": 123}' \ # Can be JSON object or eval-able Python literal
    -n 6k -r 60/min   \ # 6000 requests; 60 per minute
    --burst           \ # Send requests in burst each minute instead of 1/sec
    -l elapsed,error    # Log only elapsed time and error messages
```

## Options

```
usage: loadtest [-h] [options] METHOD URL

options:
  -h, --help            show this help message and exit
  -d EXPR_JSON, --data EXPR_JSON
                        request body as eval-able Python expression, or object
                        encoded as JSON string
  -n INT, --number INT  number of requests
  -r RATE, --rate RATE  rate of requests, e.g. 50/sec, 2.5k/min
  -b, --burst           dispatch all requests immediately at the start of the
                        given rate window instead of spreading them out evenly
                        over the second/minute
  -l FIELDS, --log FIELDS
                        comma-seperated list of fields to log
```
